/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "hardware/gpio.h"
#include "hardware/rtc.h"
#include "pico/stdlib.h"
#include "pico/util/datetime.h"

////////////////////////////////////////////////////////////////////////////////

#define PIN_LIGHTS_RELAY      28U
#define PIN_LIMIT_SWITCH      27U
#define PIN_PUMP_RELAY        26U
#define PIN_STEPPER_DIRECTION 16U
#define PIN_STEPPER_STEP      17U
#define PIN_STEPPER_ENABLE    21U

#define STEPS_PER_POSITION    (200U*16U/15U)
#define STEPS_OFFSET          (90U)

#define TIME_LIGHTS_ON        (8U*3600U)
#define TIME_LIGHTS_OFF       (20U*3600U)
#define TIME_WATER_START      (12U*3600U + 1U)
#define TIME_PUMP_RUN         30U // sec
#define TIME_PUMP_WAIT        1U  // sec

#define NUM_ACTIVE_POSITIONS  7U

////////////////////////////////////////////////////////////////////////////////

void lights_init(void);
void lights_off(void);
void lights_on(void);
void limit_switch_init(void);
bool limit_switch_read(void);
void program_update(const uint32_t now);
void pump_init(void);
void pump_on(void);
void pump_off(void);
void stepper_disable(void);
void stepper_enable(void);
void stepper_home(void);
void stepper_init(void);
void stepper_next_position(void);
void stepper_step(void);

////////////////////////////////////////////////////////////////////////////////

void lights_init(void) {
  gpio_init(PIN_LIGHTS_RELAY);
  gpio_set_dir(PIN_LIGHTS_RELAY, GPIO_OUT);
  lights_off();
}

void lights_off(void) {
  gpio_put(PIN_LIGHTS_RELAY, false);
}

void lights_on(void) {
  gpio_put(PIN_LIGHTS_RELAY, true);
}

////////////////////////////////////////////////////////////////////////////////

void limit_switch_init(void) {
  gpio_init(PIN_LIMIT_SWITCH);
  gpio_set_dir(PIN_LIMIT_SWITCH, GPIO_IN);
  gpio_pull_up(PIN_LIMIT_SWITCH);
  // gpio_set_pulls(PIN_LIMIT_SWITCH, true, false);
}

bool limit_switch_read(void) {
  return gpio_get(PIN_LIMIT_SWITCH);
}

////////////////////////////////////////////////////////////////////////////////

void pump_init(void) {
  gpio_init(PIN_PUMP_RELAY);
  gpio_set_dir(PIN_PUMP_RELAY, GPIO_OUT);
  pump_off();
}

void pump_on(void) {
  gpio_put(PIN_PUMP_RELAY, false);
}

void pump_off(void) {
  gpio_put(PIN_PUMP_RELAY, true);
}

////////////////////////////////////////////////////////////////////////////////

void stepper_disable(void) {
  gpio_put(PIN_STEPPER_ENABLE, true);
}

void stepper_enable(void) {
  gpio_put(PIN_STEPPER_ENABLE, false);
}

void stepper_home(void) {
  gpio_put(PIN_STEPPER_DIRECTION, false);

  while(limit_switch_read()) {
    stepper_step();
  }

  gpio_put(PIN_STEPPER_DIRECTION, true);

  for(uint32_t i = 0U; i < STEPS_OFFSET; i++) {
    stepper_step();
  }
}

void stepper_init(void) {
  gpio_init(PIN_STEPPER_DIRECTION);
  gpio_set_dir(PIN_STEPPER_DIRECTION, GPIO_OUT);
  gpio_put(PIN_STEPPER_DIRECTION, false);

  gpio_init(PIN_STEPPER_STEP);
  gpio_set_dir(PIN_STEPPER_STEP, GPIO_OUT);
  gpio_put(PIN_STEPPER_STEP, false);

  gpio_init(PIN_STEPPER_ENABLE);
  gpio_set_dir(PIN_STEPPER_ENABLE, GPIO_OUT);
  gpio_put(PIN_STEPPER_ENABLE, true);
}

void stepper_next_position(void) {
  gpio_put(PIN_STEPPER_DIRECTION, true);

  for (uint32_t i = 0U; i < STEPS_PER_POSITION; i++) {
    stepper_step();
  }
}

void stepper_step(void) {
  gpio_put(PIN_STEPPER_STEP, true);
  sleep_us(10U);
  gpio_put(PIN_STEPPER_STEP, false);
  sleep_ms(5U);
}

////////////////////////////////////////////////////////////////////////////////

void program_update(const uint32_t now) {
  // Update Grow Lights
  if ((now > TIME_LIGHTS_ON) && (now < TIME_LIGHTS_OFF)) {
    lights_on();
  }
  else {
    lights_off();
  }

  // Update Watering System
  if (now == TIME_WATER_START) {
    stepper_enable();
    stepper_home();

    for (uint32_t position = 1U; position <= NUM_ACTIVE_POSITIONS; position++) {
      sleep_ms(TIME_PUMP_WAIT*1000U);
      pump_on();
      sleep_ms(TIME_PUMP_RUN*1000U);
      pump_off();
      sleep_ms(TIME_PUMP_WAIT*1000U);

      if (position != NUM_ACTIVE_POSITIONS) {
        stepper_next_position();
      }
    }

    stepper_disable();
  }
}

////////////////////////////////////////////////////////////////////////////////

int main() {

  datetime_t now = {
    .year  = 2000,
    .month = 01,
    .day   = 01,
    .dotw  = 00,
    .hour  = 12,
    .min   = 00,
    .sec   = 00
  };

  rtc_init();
  rtc_set_datetime(&now);

  lights_init();
  limit_switch_init();
  pump_init();
  stepper_init();

  uint32_t prev_sec = now.sec;

  while(true) {
    rtc_get_datetime(&now);

    if(now.sec != prev_sec) {
      prev_sec = now.sec;

      const uint32_t time = now.hour*3600U + now.min*60U + now.sec;
      program_update(time);
    }
    else {
      sleep_ms(200U);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////
// End of file
////////////////////////////////////////////////////////////////////////////////
